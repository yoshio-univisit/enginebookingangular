"use strict";
angular.module('EngineWidgets', ['EngineCore']).
config(['$provide', function ($provide) {
	
    $provide.factory('BookingBoxConfig', [function () {
		return {
			MIN_ROOMS: 1,
			MAX_ROOMS: 10,
			MIN_ADULTS: 1,
			MAX_ADULTS: 4,
			MIN_CHILDREN: 0,
			MAX_CHILDREN: 3,
			MIN_CHILDREN_AGES: 0,
			MAX_CHILDREN_AGES: 13
		};
	}]);
    
    $provide.constant('paginationConfig', {
        itemsPerPage: 10,
        boundaryLinks: false,
        directionLinks: true,
        firstText: 'First',
        previousText: '‹',
        nextText: '›',
        lastText: 'Last',
        rotate: false
    });
    
    $provide.constant('countriesOptions',{
        'en':[{"name":"Afghanistan","name_country_phone_code":"Afghanistan (+93)","country_code":"AF","dial_code":"+93"},{"name":"Albania","name_country_phone_code":"Albania (+355)","country_code":"AL","dial_code":"+355"},{"name":"Algeria","name_country_phone_code":"Algeria (+213)","country_code":"DZ","dial_code":"+213"},{"name":"AmericanSamoa","name_country_phone_code":"AmericanSamoa (+1 684)","country_code":"AS","dial_code":"+1 684"},{"name":"Andorra","name_country_phone_code":"Andorra (+376)","country_code":"AD","dial_code":"+376"},{"name":"Angola","name_country_phone_code":"Angola (+244)","country_code":"AO","dial_code":"+244"},{"name":"Anguilla","name_country_phone_code":"Anguilla (+1 264)","country_code":"AI","dial_code":"+1 264"},{"name":"Antarctica","name_country_phone_code":"Antarctica (+672)","country_code":"AQ","dial_code":"+672"},{"name":"Antigua and Barbuda","name_country_phone_code":"Antigua and Barbuda (+1268)","country_code":"AG","dial_code":"+1268"},{"name":"Argentina","name_country_phone_code":"Argentina (+54)","country_code":"AR","dial_code":"+54"},{"name":"Armenia","name_country_phone_code":"Armenia (+374)","country_code":"AM","dial_code":"+374"},{"name":"Aruba","name_country_phone_code":"Aruba (+297)","country_code":"AW","dial_code":"+297"},{"name":"Australia","name_country_phone_code":"Australia (+61)","country_code":"AU","dial_code":"+61"},{"name":"Austria","name_country_phone_code":"Austria (+43)","country_code":"AT","dial_code":"+43"},{"name":"Azerbaijan","name_country_phone_code":"Azerbaijan (+994)","country_code":"AZ","dial_code":"+994"},{"name":"Bahamas","name_country_phone_code":"Bahamas (+1 242)","country_code":"BS","dial_code":"+1 242"},{"name":"Bahrain","name_country_phone_code":"Bahrain (+973)","country_code":"BH","dial_code":"+973"},{"name":"Bangladesh","name_country_phone_code":"Bangladesh (+880)","country_code":"BD","dial_code":"+880"},{"name":"Barbados","name_country_phone_code":"Barbados (+1 246)","country_code":"BB","dial_code":"+1 246"},{"name":"Belarus","name_country_phone_code":"Belarus (+375)","country_code":"BY","dial_code":"+375"},{"name":"Belgium","name_country_phone_code":"Belgium (+32)","country_code":"BE","dial_code":"+32"},{"name":"Belize","name_country_phone_code":"Belize (+501)","country_code":"BZ","dial_code":"+501"},{"name":"Benin","name_country_phone_code":"Benin (+229)","country_code":"BJ","dial_code":"+229"},{"name":"Bermuda","name_country_phone_code":"Bermuda (+1 441)","country_code":"BM","dial_code":"+1 441"},{"name":"Bhutan","name_country_phone_code":"Bhutan (+975)","country_code":"BT","dial_code":"+975"},{"name":"Bolivia, Plurinational State of","name_country_phone_code":"Bolivia, Plurinational State of (+591)","country_code":"BO","dial_code":"+591"},{"name":"Bosnia and Herzegovina","name_country_phone_code":"Bosnia and Herzegovina (+387)","country_code":"BA","dial_code":"+387"},{"name":"Botswana","name_country_phone_code":"Botswana (+267)","country_code":"BW","dial_code":"+267"},{"name":"Brazil","name_country_phone_code":"Brazil (+55)","country_code":"BR","dial_code":"+55"},{"name":"British Indian Ocean Territory","name_country_phone_code":"British Indian Ocean Territory (+246)","country_code":"IO","dial_code":"+246"},{"name":"Brunei Darussalam","name_country_phone_code":"Brunei Darussalam (+673)","country_code":"BN","dial_code":"+673"},{"name":"Bulgaria","name_country_phone_code":"Bulgaria (+359)","country_code":"BG","dial_code":"+359"},{"name":"Burkina Faso","name_country_phone_code":"Burkina Faso (+226)","country_code":"BF","dial_code":"+226"},{"name":"Burundi","name_country_phone_code":"Burundi (+257)","country_code":"BI","dial_code":"+257"},{"name":"Cambodia","name_country_phone_code":"Cambodia (+855)","country_code":"KH","dial_code":"+855"},{"name":"Cameroon","name_country_phone_code":"Cameroon (+237)","country_code":"CM","dial_code":"+237"},{"name":"Canada","name_country_phone_code":"Canada (+1)","country_code":"CA","dial_code":"+1"},{"name":"Cape Verde","name_country_phone_code":"Cape Verde (+238)","country_code":"CV","dial_code":"+238"},{"name":"Cayman Islands","name_country_phone_code":"Cayman Islands (+ 345)","country_code":"KY","dial_code":"+ 345"},{"name":"Central African Republic","name_country_phone_code":"Central African Republic (+236)","country_code":"CF","dial_code":"+236"},{"name":"Chad","name_country_phone_code":"Chad (+235)","country_code":"TD","dial_code":"+235"},{"name":"Chile","name_country_phone_code":"Chile (+56)","country_code":"CL","dial_code":"+56"},{"name":"China","name_country_phone_code":"China (+86)","country_code":"CN","dial_code":"+86"},{"name":"Christmas Island","name_country_phone_code":"Christmas Island (+61)","country_code":"CX","dial_code":"+61"},{"name":"Cocos (Keeling) Islands","name_country_phone_code":"Cocos (Keeling) Islands (+61)","country_code":"CC","dial_code":"+61"},{"name":"Colombia","name_country_phone_code":"Colombia (+57)","country_code":"CO","dial_code":"+57"},{"name":"Comoros","name_country_phone_code":"Comoros (+269)","country_code":"KM","dial_code":"+269"},{"name":"Congo","name_country_phone_code":"Congo (+242)","country_code":"CG","dial_code":"+242"},{"name":"Congo, The Democratic Republic of the","name_country_phone_code":"Congo, The Democratic Republic of the (+243)","country_code":"CD","dial_code":"+243"},{"name":"Cook Islands","name_country_phone_code":"Cook Islands (+682)","country_code":"CK","dial_code":"+682"},{"name":"Costa Rica","name_country_phone_code":"Costa Rica (+506)","country_code":"CR","dial_code":"+506"},{"name":"Cote d'Ivoire","name_country_phone_code":"Cote d'Ivoire (+225)","country_code":"CI","dial_code":"+225"},{"name":"Croatia","name_country_phone_code":"Croatia (+385)","country_code":"HR","dial_code":"+385"},{"name":"Cuba","name_country_phone_code":"Cuba (+53)","country_code":"CU","dial_code":"+53"},{"name":"Cyprus","name_country_phone_code":"Cyprus (+537)","country_code":"CY","dial_code":"+537"},{"name":"Czech Republic","name_country_phone_code":"Czech Republic (+420)","country_code":"CZ","dial_code":"+420"},{"name":"Denmark","name_country_phone_code":"Denmark (+45)","country_code":"DK","dial_code":"+45"},{"name":"Djibouti","name_country_phone_code":"Djibouti (+253)","country_code":"DJ","dial_code":"+253"},{"name":"Dominica","name_country_phone_code":"Dominica (+1 767)","country_code":"DM","dial_code":"+1 767"},{"name":"Dominican Republic","name_country_phone_code":"Dominican Republic (+1 849)","country_code":"DO","dial_code":"+1 849"},{"name":"Ecuador","name_country_phone_code":"Ecuador (+593)","country_code":"EC","dial_code":"+593"},{"name":"Egypt","name_country_phone_code":"Egypt (+20)","country_code":"EG","dial_code":"+20"},{"name":"El Salvador","name_country_phone_code":"El Salvador (+503)","country_code":"SV","dial_code":"+503"},{"name":"Equatorial Guinea","name_country_phone_code":"Equatorial Guinea (+240)","country_code":"GQ","dial_code":"+240"},{"name":"Eritrea","name_country_phone_code":"Eritrea (+291)","country_code":"ER","dial_code":"+291"},{"name":"Estonia","name_country_phone_code":"Estonia (+372)","country_code":"EE","dial_code":"+372"},{"name":"Ethiopia","name_country_phone_code":"Ethiopia (+251)","country_code":"ET","dial_code":"+251"},{"name":"Falkland Islands (Malvinas)","name_country_phone_code":"Falkland Islands (Malvinas) (+500)","country_code":"FK","dial_code":"+500"},{"name":"Faroe Islands","name_country_phone_code":"Faroe Islands (+298)","country_code":"FO","dial_code":"+298"},{"name":"Fiji","name_country_phone_code":"Fiji (+679)","country_code":"FJ","dial_code":"+679"},{"name":"Finland","name_country_phone_code":"Finland (+358)","country_code":"FI","dial_code":"+358"},{"name":"France","name_country_phone_code":"France (+33)","country_code":"FR","dial_code":"+33"},{"name":"French Guiana","name_country_phone_code":"French Guiana (+594)","country_code":"GF","dial_code":"+594"},{"name":"French Polynesia","name_country_phone_code":"French Polynesia (+689)","country_code":"PF","dial_code":"+689"},{"name":"Gabon","name_country_phone_code":"Gabon (+241)","country_code":"GA","dial_code":"+241"},{"name":"Gambia","name_country_phone_code":"Gambia (+220)","country_code":"GM","dial_code":"+220"},{"name":"Georgia","name_country_phone_code":"Georgia (+995)","country_code":"GE","dial_code":"+995"},{"name":"Germany","name_country_phone_code":"Germany (+49)","country_code":"DE","dial_code":"+49"},{"name":"Ghana","name_country_phone_code":"Ghana (+233)","country_code":"GH","dial_code":"+233"},{"name":"Gibraltar","name_country_phone_code":"Gibraltar (+350)","country_code":"GI","dial_code":"+350"},{"name":"Greece","name_country_phone_code":"Greece (+30)","country_code":"GR","dial_code":"+30"},{"name":"Greenland","name_country_phone_code":"Greenland (+299)","country_code":"GL","dial_code":"+299"},{"name":"Grenada","name_country_phone_code":"Grenada (+1 473)","country_code":"GD","dial_code":"+1 473"},{"name":"Guadeloupe","name_country_phone_code":"Guadeloupe (+590)","country_code":"GP","dial_code":"+590"},{"name":"Guam","name_country_phone_code":"Guam (+1 671)","country_code":"GU","dial_code":"+1 671"},{"name":"Guatemala","name_country_phone_code":"Guatemala (+502)","country_code":"GT","dial_code":"+502"},{"name":"Guernsey","name_country_phone_code":"Guernsey (+44)","country_code":"GG","dial_code":"+44"},{"name":"Guinea","name_country_phone_code":"Guinea (+224)","country_code":"GN","dial_code":"+224"},{"name":"Guinea-Bissau","name_country_phone_code":"Guinea-Bissau (+245)","country_code":"GW","dial_code":"+245"},{"name":"Guyana","name_country_phone_code":"Guyana (+595)","country_code":"GY","dial_code":"+595"},{"name":"Haiti","name_country_phone_code":"Haiti (+509)","country_code":"HT","dial_code":"+509"},{"name":"Holy See (Vatican City State)","name_country_phone_code":"Holy See (Vatican City State) (+379)","country_code":"VA","dial_code":"+379"},{"name":"Honduras","name_country_phone_code":"Honduras (+504)","country_code":"HN","dial_code":"+504"},{"name":"Hong Kong","name_country_phone_code":"Hong Kong (+852)","country_code":"HK","dial_code":"+852"},{"name":"Hungary","name_country_phone_code":"Hungary (+36)","country_code":"HU","dial_code":"+36"},{"name":"Iceland","name_country_phone_code":"Iceland (+354)","country_code":"IS","dial_code":"+354"},{"name":"India","name_country_phone_code":"India (+91)","country_code":"IN","dial_code":"+91"},{"name":"Indonesia","name_country_phone_code":"Indonesia (+62)","country_code":"ID","dial_code":"+62"},{"name":"Iran, Islamic Republic of","name_country_phone_code":"Iran, Islamic Republic of (+98)","country_code":"IR","dial_code":"+98"},{"name":"Iraq","name_country_phone_code":"Iraq (+964)","country_code":"IQ","dial_code":"+964"},{"name":"Ireland","name_country_phone_code":"Ireland (+353)","country_code":"IE","dial_code":"+353"},{"name":"Isle of Man","name_country_phone_code":"Isle of Man (+44)","country_code":"IM","dial_code":"+44"},{"name":"Israel","name_country_phone_code":"Israel (+972)","country_code":"IL","dial_code":"+972"},{"name":"Italy","name_country_phone_code":"Italy (+39)","country_code":"IT","dial_code":"+39"},{"name":"Jamaica","name_country_phone_code":"Jamaica (+1 876)","country_code":"JM","dial_code":"+1 876"},{"name":"Japan","name_country_phone_code":"Japan (+81)","country_code":"JP","dial_code":"+81"},{"name":"Jersey","name_country_phone_code":"Jersey (+44)","country_code":"JE","dial_code":"+44"},{"name":"Jordan","name_country_phone_code":"Jordan (+962)","country_code":"JO","dial_code":"+962"},{"name":"Kazakhstan","name_country_phone_code":"Kazakhstan (+7 7)","country_code":"KZ","dial_code":"+7 7"},{"name":"Kenya","name_country_phone_code":"Kenya (+254)","country_code":"KE","dial_code":"+254"},{"name":"Kiribati","name_country_phone_code":"Kiribati (+686)","country_code":"KI","dial_code":"+686"},{"name":"Korea, Democratic People's Republic of","name_country_phone_code":"Korea, Democratic People's Republic of (+850)","country_code":"KP","dial_code":"+850"},{"name":"Korea, Republic of","name_country_phone_code":"Korea, Republic of (+82)","country_code":"KR","dial_code":"+82"},{"name":"Kuwait","name_country_phone_code":"Kuwait (+965)","country_code":"KW","dial_code":"+965"},{"name":"Kyrgyzstan","name_country_phone_code":"Kyrgyzstan (+996)","country_code":"KG","dial_code":"+996"},{"name":"Lao People's Democratic Republic","name_country_phone_code":"Lao People's Democratic Republic (+856)","country_code":"LA","dial_code":"+856"},{"name":"Latvia","name_country_phone_code":"Latvia (+371)","country_code":"LV","dial_code":"+371"},{"name":"Lebanon","name_country_phone_code":"Lebanon (+961)","country_code":"LB","dial_code":"+961"},{"name":"Lesotho","name_country_phone_code":"Lesotho (+266)","country_code":"LS","dial_code":"+266"},{"name":"Liberia","name_country_phone_code":"Liberia (+231)","country_code":"LR","dial_code":"+231"},{"name":"Libyan Arab Jamahiriya","name_country_phone_code":"Libyan Arab Jamahiriya (+218)","country_code":"LY","dial_code":"+218"},{"name":"Liechtenstein","name_country_phone_code":"Liechtenstein (+423)","country_code":"LI","dial_code":"+423"},{"name":"Lithuania","name_country_phone_code":"Lithuania (+370)","country_code":"LT","dial_code":"+370"},{"name":"Luxembourg","name_country_phone_code":"Luxembourg (+352)","country_code":"LU","dial_code":"+352"},{"name":"Macao","name_country_phone_code":"Macao (+853)","country_code":"MO","dial_code":"+853"},{"name":"Macedonia, The Former Yugoslav Republic of","name_country_phone_code":"Macedonia, The Former Yugoslav Republic of (+389)","country_code":"MK","dial_code":"+389"},{"name":"Madagascar","name_country_phone_code":"Madagascar (+261)","country_code":"MG","dial_code":"+261"},{"name":"Malawi","name_country_phone_code":"Malawi (+265)","country_code":"MW","dial_code":"+265"},{"name":"Malaysia","name_country_phone_code":"Malaysia (+60)","country_code":"MY","dial_code":"+60"},{"name":"Maldives","name_country_phone_code":"Maldives (+960)","country_code":"MV","dial_code":"+960"},{"name":"Mali","name_country_phone_code":"Mali (+223)","country_code":"ML","dial_code":"+223"},{"name":"Malta","name_country_phone_code":"Malta (+356)","country_code":"MT","dial_code":"+356"},{"name":"Marshall Islands","name_country_phone_code":"Marshall Islands (+692)","country_code":"MH","dial_code":"+692"},{"name":"Martinique","name_country_phone_code":"Martinique (+596)","country_code":"MQ","dial_code":"+596"},{"name":"Mauritania","name_country_phone_code":"Mauritania (+222)","country_code":"MR","dial_code":"+222"},{"name":"Mauritius","name_country_phone_code":"Mauritius (+230)","country_code":"MU","dial_code":"+230"},{"name":"Mayotte","name_country_phone_code":"Mayotte (+262)","country_code":"YT","dial_code":"+262"},{"name":"Mexico","name_country_phone_code":"Mexico (+52)","country_code":"MX","dial_code":"+52"},{"name":"Micronesia, Federated States of","name_country_phone_code":"Micronesia, Federated States of (+691)","country_code":"FM","dial_code":"+691"},{"name":"Moldova, Republic of","name_country_phone_code":"Moldova, Republic of (+373)","country_code":"MD","dial_code":"+373"},{"name":"Monaco","name_country_phone_code":"Monaco (+377)","country_code":"MC","dial_code":"+377"},{"name":"Mongolia","name_country_phone_code":"Mongolia (+976)","country_code":"MN","dial_code":"+976"},{"name":"Montenegro","name_country_phone_code":"Montenegro (+382)","country_code":"ME","dial_code":"+382"},{"name":"Montserrat","name_country_phone_code":"Montserrat (+1664)","country_code":"MS","dial_code":"+1664"},{"name":"Morocco","name_country_phone_code":"Morocco (+212)","country_code":"MA","dial_code":"+212"},{"name":"Mozambique","name_country_phone_code":"Mozambique (+258)","country_code":"MZ","dial_code":"+258"},{"name":"Myanmar","name_country_phone_code":"Myanmar (+95)","country_code":"MM","dial_code":"+95"},{"name":"Namibia","name_country_phone_code":"Namibia (+264)","country_code":"NA","dial_code":"+264"},{"name":"Nauru","name_country_phone_code":"Nauru (+674)","country_code":"NR","dial_code":"+674"},{"name":"Nepal","name_country_phone_code":"Nepal (+977)","country_code":"NP","dial_code":"+977"},{"name":"Netherlands","name_country_phone_code":"Netherlands (+31)","country_code":"NL","dial_code":"+31"},{"name":"Netherlands Antilles","name_country_phone_code":"Netherlands Antilles (+599)","country_code":"AN","dial_code":"+599"},{"name":"New Caledonia","name_country_phone_code":"New Caledonia (+687)","country_code":"NC","dial_code":"+687"},{"name":"New Zealand","name_country_phone_code":"New Zealand (+64)","country_code":"NZ","dial_code":"+64"},{"name":"Nicaragua","name_country_phone_code":"Nicaragua (+505)","country_code":"NI","dial_code":"+505"},{"name":"Niger","name_country_phone_code":"Niger (+227)","country_code":"NE","dial_code":"+227"},{"name":"Nigeria","name_country_phone_code":"Nigeria (+234)","country_code":"NG","dial_code":"+234"},{"name":"Niue","name_country_phone_code":"Niue (+683)","country_code":"NU","dial_code":"+683"},{"name":"Norfolk Island","name_country_phone_code":"Norfolk Island (+672)","country_code":"NF","dial_code":"+672"},{"name":"Northern Mariana Islands","name_country_phone_code":"Northern Mariana Islands (+1 670)","country_code":"MP","dial_code":"+1 670"},{"name":"Norway","name_country_phone_code":"Norway (+47)","country_code":"NO","dial_code":"+47"},{"name":"Oman","name_country_phone_code":"Oman (+968)","country_code":"OM","dial_code":"+968"},{"name":"Pakistan","name_country_phone_code":"Pakistan (+92)","country_code":"PK","dial_code":"+92"},{"name":"Palau","name_country_phone_code":"Palau (+680)","country_code":"PW","dial_code":"+680"},{"name":"Palestinian Territory, Occupied","name_country_phone_code":"Palestinian Territory, Occupied (+970)","country_code":"PS","dial_code":"+970"},{"name":"Panama","name_country_phone_code":"Panama (+507)","country_code":"PA","dial_code":"+507"},{"name":"Papua New Guinea","name_country_phone_code":"Papua New Guinea (+675)","country_code":"PG","dial_code":"+675"},{"name":"Paraguay","name_country_phone_code":"Paraguay (+595)","country_code":"PY","dial_code":"+595"},{"name":"Peru","name_country_phone_code":"Peru (+51)","country_code":"PE","dial_code":"+51"},{"name":"Philippines","name_country_phone_code":"Philippines (+63)","country_code":"PH","dial_code":"+63"},{"name":"Pitcairn","name_country_phone_code":"Pitcairn (+872)","country_code":"PN","dial_code":"+872"},{"name":"Poland","name_country_phone_code":"Poland (+48)","country_code":"PL","dial_code":"+48"},{"name":"Portugal","name_country_phone_code":"Portugal (+351)","country_code":"PT","dial_code":"+351"},{"name":"Puerto Rico","name_country_phone_code":"Puerto Rico (+1 939)","country_code":"PR","dial_code":"+1 939"},{"name":"Qatar","name_country_phone_code":"Qatar (+974)","country_code":"QA","dial_code":"+974"},{"name":"Romania","name_country_phone_code":"Romania (+40)","country_code":"RO","dial_code":"+40"},{"name":"Russia","name_country_phone_code":"Russia (+7)","country_code":"RU","dial_code":"+7"},{"name":"Rwanda","name_country_phone_code":"Rwanda (+250)","country_code":"RW","dial_code":"+250"},{"name":"Réunion","name_country_phone_code":"Réunion (+262)","country_code":"RE","dial_code":"+262"},{"name":"Saint Barthélemy","name_country_phone_code":"Saint Barthélemy (+590)","country_code":"BL","dial_code":"+590"},{"name":"Saint Helena, Ascension and Tristan Da Cunha","name_country_phone_code":"Saint Helena, Ascension and Tristan Da Cunha (+290)","country_code":"SH","dial_code":"+290"},{"name":"Saint Kitts and Nevis","name_country_phone_code":"Saint Kitts and Nevis (+1 869)","country_code":"KN","dial_code":"+1 869"},{"name":"Saint Lucia","name_country_phone_code":"Saint Lucia (+1 758)","country_code":"LC","dial_code":"+1 758"},{"name":"Saint Martin","name_country_phone_code":"Saint Martin (+590)","country_code":"MF","dial_code":"+590"},{"name":"Saint Pierre and Miquelon","name_country_phone_code":"Saint Pierre and Miquelon (+508)","country_code":"PM","dial_code":"+508"},{"name":"Saint Vincent and the Grenadines","name_country_phone_code":"Saint Vincent and the Grenadines (+1 784)","country_code":"VC","dial_code":"+1 784"},{"name":"Samoa","name_country_phone_code":"Samoa (+685)","country_code":"WS","dial_code":"+685"},{"name":"San Marino","name_country_phone_code":"San Marino (+378)","country_code":"SM","dial_code":"+378"},{"name":"Sao Tome and Principe","name_country_phone_code":"Sao Tome and Principe (+239)","country_code":"ST","dial_code":"+239"},{"name":"Saudi Arabia","name_country_phone_code":"Saudi Arabia (+966)","country_code":"SA","dial_code":"+966"},{"name":"Senegal","name_country_phone_code":"Senegal (+221)","country_code":"SN","dial_code":"+221"},{"name":"Serbia","name_country_phone_code":"Serbia (+381)","country_code":"RS","dial_code":"+381"},{"name":"Seychelles","name_country_phone_code":"Seychelles (+248)","country_code":"SC","dial_code":"+248"},{"name":"Sierra Leone","name_country_phone_code":"Sierra Leone (+232)","country_code":"SL","dial_code":"+232"},{"name":"Singapore","name_country_phone_code":"Singapore (+65)","country_code":"SG","dial_code":"+65"},{"name":"Slovakia","name_country_phone_code":"Slovakia (+421)","country_code":"SK","dial_code":"+421"},{"name":"Slovenia","name_country_phone_code":"Slovenia (+386)","country_code":"SI","dial_code":"+386"},{"name":"Solomon Islands","name_country_phone_code":"Solomon Islands (+677)","country_code":"SB","dial_code":"+677"},{"name":"Somalia","name_country_phone_code":"Somalia (+252)","country_code":"SO","dial_code":"+252"},{"name":"South Africa","name_country_phone_code":"South Africa (+27)","country_code":"ZA","dial_code":"+27"},{"name":"South Georgia and the South Sandwich Islands","name_country_phone_code":"South Georgia and the South Sandwich Islands (+500)","country_code":"GS","dial_code":"+500"},{"name":"Spain","name_country_phone_code":"Spain (+34)","country_code":"ES","dial_code":"+34"},{"name":"Sri Lanka","name_country_phone_code":"Sri Lanka (+94)","country_code":"LK","dial_code":"+94"},{"name":"Sudan","name_country_phone_code":"Sudan (+249)","country_code":"SD","dial_code":"+249"},{"name":"Suriname","name_country_phone_code":"Suriname (+597)","country_code":"SR","dial_code":"+597"},{"name":"Svalbard and Jan Mayen","name_country_phone_code":"Svalbard and Jan Mayen (+47)","country_code":"SJ","dial_code":"+47"},{"name":"Swaziland","name_country_phone_code":"Swaziland (+268)","country_code":"SZ","dial_code":"+268"},{"name":"Sweden","name_country_phone_code":"Sweden (+46)","country_code":"SE","dial_code":"+46"},{"name":"Switzerland","name_country_phone_code":"Switzerland (+41)","country_code":"CH","dial_code":"+41"},{"name":"Syrian Arab Republic","name_country_phone_code":"Syrian Arab Republic (+963)","country_code":"SY","dial_code":"+963"},{"name":"Taiwan, Province of China","name_country_phone_code":"Taiwan, Province of China (+886)","country_code":"TW","dial_code":"+886"},{"name":"Tajikistan","name_country_phone_code":"Tajikistan (+992)","country_code":"TJ","dial_code":"+992"},{"name":"Tanzania, United Republic of","name_country_phone_code":"Tanzania, United Republic of (+255)","country_code":"TZ","dial_code":"+255"},{"name":"Thailand","name_country_phone_code":"Thailand (+66)","country_code":"TH","dial_code":"+66"},{"name":"Timor-Leste","name_country_phone_code":"Timor-Leste (+670)","country_code":"TL","dial_code":"+670"},{"name":"Togo","name_country_phone_code":"Togo (+228)","country_code":"TG","dial_code":"+228"},{"name":"Tokelau","name_country_phone_code":"Tokelau (+690)","country_code":"TK","dial_code":"+690"},{"name":"Tonga","name_country_phone_code":"Tonga (+676)","country_code":"TO","dial_code":"+676"},{"name":"Trinidad and Tobago","name_country_phone_code":"Trinidad and Tobago (+1 868)","country_code":"TT","dial_code":"+1 868"},{"name":"Tunisia","name_country_phone_code":"Tunisia (+216)","country_code":"TN","dial_code":"+216"},{"name":"Turkey","name_country_phone_code":"Turkey (+90)","country_code":"TR","dial_code":"+90"},{"name":"Turkmenistan","name_country_phone_code":"Turkmenistan (+993)","country_code":"TM","dial_code":"+993"},{"name":"Turks and Caicos Islands","name_country_phone_code":"Turks and Caicos Islands (+1 649)","country_code":"TC","dial_code":"+1 649"},{"name":"Tuvalu","name_country_phone_code":"Tuvalu (+688)","country_code":"TV","dial_code":"+688"},{"name":"Uganda","name_country_phone_code":"Uganda (+256)","country_code":"UG","dial_code":"+256"},{"name":"Ukraine","name_country_phone_code":"Ukraine (+380)","country_code":"UA","dial_code":"+380"},{"name":"United Arab Emirates","name_country_phone_code":"United Arab Emirates (+971)","country_code":"AE","dial_code":"+971"},{"name":"United Kingdom","name_country_phone_code":"United Kingdom (+44)","country_code":"GB","dial_code":"+44"},{"name":"United States","name_country_phone_code":"United States (+1)","country_code":"US","dial_code":"+1"},{"name":"Uruguay","name_country_phone_code":"Uruguay (+598)","country_code":"UY","dial_code":"+598"},{"name":"Uzbekistan","name_country_phone_code":"Uzbekistan (+998)","country_code":"UZ","dial_code":"+998"},{"name":"Vanuatu","name_country_phone_code":"Vanuatu (+678)","country_code":"VU","dial_code":"+678"},{"name":"Venezuela, Bolivarian Republic of","name_country_phone_code":"Venezuela, Bolivarian Republic of (+58)","country_code":"VE","dial_code":"+58"},{"name":"Viet Nam","name_country_phone_code":"Viet Nam (+84)","country_code":"VN","dial_code":"+84"},{"name":"Virgin Islands, British","name_country_phone_code":"Virgin Islands, British (+1 284)","country_code":"VG","dial_code":"+1 284"},{"name":"Virgin Islands, U.S.","name_country_phone_code":"Virgin Islands, U.S. (+1 340)","country_code":"VI","dial_code":"+1 340"},{"name":"Wallis and Futuna","name_country_phone_code":"Wallis and Futuna (+681)","country_code":"WF","dial_code":"+681"},{"name":"Yemen","name_country_phone_code":"Yemen (+967)","country_code":"YE","dial_code":"+967"},{"name":"Zambia","name_country_phone_code":"Zambia (+260)","country_code":"ZM","dial_code":"+260"},{"name":"Zimbabwe","name_country_phone_code":"Zimbabwe (+263)","country_code":"ZW","dial_code":"+263"},{"name":"Åland Islands","name_country_phone_code":"Åland Islands (+358)","country_code":"AX","dial_code":"+358"}]
    })
    
    //SAFETY_PASSWD_PATTERN al menos 8 caracteres, al menos 1 numero, al menos 1 minuscula, al menos 1 mayuscula
    $provide.constant('FORMS_VALIDATION_PATTERN', {
        'NAME_PATTERN':/^[a-zA-Z.,\ \'\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC]+$/,
        'EMAIL_PATTERN':/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm,
        'ALPHANUMERIC_PATTERN':/^[a-zA-Z0-9,._\-\ \'\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC]+$/,
        'PHONE_PATTERN':/^[0-9]{7,10}$/,
        'PASSWD_PATTERN':/^[a-zA-Z0-9\ \'\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC]{4,9}$/,
        'SAFETY_PASSWD_PATTERN':/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
        'SECURITY_CODE_PATTERN':/^[0-9]{3,4}$/,
        'VISA_PATTERN':/^4[0-9]{12}(?:[0-9]{3})?$/,
        'MASTERCARD_PATTERN':/^5[1-5][0-9]{14}$/,
        'AMERICANEXPRESS_PATTERN': /^3[47][0-9]{13}$/,
        'EXPIRATION_DATE_PATTERN':/^((0[1-9])|(1[0-2]))\/(\d{4})$/,
        'EMPTY_PATTERN':/\S+/,
        
    });
}]).
directive('nameWidget', ['FORMS_VALIDATION_PATTERN', function(FORMS_VALIDATION_PATTERN){
    return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){            
            ctrl.$parsers.push(function(viewValue){
                var isValid = true;
                isValid = (viewValue === null || typeof viewValue === 'undefined' || viewValue === '');
                if(!isValid){
                    isValid = typeof viewValue === 'string'
                    && FORMS_VALIDATION_PATTERN.NAME_PATTERN.test(viewValue);
                }
                ctrl.$setValidity('nameWidget', isValid);
                (isValid)?element.removeClass('error-input'):element.addClass('error-input')
                return isValid ? viewValue : undefined;
            });
            
            scope.$watch(attrs.ngModel, function(value){
                ctrl.$setViewValue(value);
            },true)
            
        }
    }
}]).
directive('emailWidget',['FORMS_VALIDATION_PATTERN', function(FORMS_VALIDATION_PATTERN){
     return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){
            ctrl.$parsers.push(function(viewValue){
                var isValid = true;
                isValid = (viewValue === null || typeof viewValue === 'undefined' || viewValue === '');
                if(!isValid){
                    isValid = typeof viewValue === 'string'
                    && FORMS_VALIDATION_PATTERN.EMAIL_PATTERN.test(viewValue);
                }
                ctrl.$setValidity('emailWidget', isValid);
                (isValid)?element.removeClass('error-input'):element.addClass('error-input')
                return isValid ? viewValue : undefined;
            });
            
            scope.$watch(attrs.ngModel, function(value){
                ctrl.$setViewValue(value);
            },true)
        }
    }
}]).
directive('emailConfirmationWidget',['FORMS_VALIDATION_PATTERN', function(FORMS_VALIDATION_PATTERN){
     return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){
            ctrl.$parsers.push(function(viewValue){
                var isValid = true;
                isValid = (viewValue === null || typeof viewValue === 'undefined' || viewValue === '');
                if(!isValid){
                    isValid = typeof viewValue === 'string'
                    && FORMS_VALIDATION_PATTERN.EMAIL_PATTERN.test(viewValue);
                }
                ctrl.$setValidity('emailWidget', isValid);
                (isValid)?element.removeClass('error-input'):element.addClass('error-input')
                return isValid ? viewValue : undefined;
            });
            
            scope.$watch(attrs.ngModel, function(value){
                ctrl.$setViewValue(value);
            },true)
        }
    }
}]).

directive('creditCardWidget', [function(){
     return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){
            ctrl.$setViewValue(attrs.value);
        }
    }
}]).
directive('securityCodeWidget',[function(){
    return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){
            ctrl.$setViewValue(attrs.value);
        }
    }
}]).
directive('phoneWidget',['FORMS_VALIDATION_PATTERN', function(FORMS_VALIDATION_PATTERN){
    return{
        restrict:'AEC',
        require:'?ngModel',
        link:function(scope, element, attrs, ctrl){
            
             ctrl.$formatters.push(function(modelValue){
               var isValid = typeof modelValue === 'string'
                    && FORMS_VALIDATION_PATTERN.PHONE_PATTERN.test(modelValue);
                ctrl.$setValidity('phoneWidget', isValid);
                return modelValue;
            });
            
            ctrl.$parsers.push(function(viewValue){
                var isValid = true;
                isValid = (viewValue === null || typeof viewValue === 'undefined' || viewValue === '');
                if(!isValid){
                    isValid = typeof viewValue === 'string'
                    && FORMS_VALIDATION_PATTERN.PHONE_PATTERN.test(viewValue);
                }
                ctrl.$setValidity('phoneWidget', isValid);
                (isValid)?element.removeClass('error-input'):element.addClass('error-input')
                return isValid ? viewValue : undefined;
            });
            
            scope.$watch(attrs.ngModel, function(value){
                ctrl.$setViewValue(value);
            },true)
        }
    }
}]).
directive('datePickerInput', [function () {
	return {
		restrict: 'AEC',
		require:'ngModel',
		link: function (scope, element, iAttrs, ctrl) {
			element.datepicker({
				dateFormat:'dd/mm/yy',
				minDate:0,
				endDate:'',
				numberOfMonths:2,
				onSelect:function(text){
					ctrl.$setViewValue(text);
				}
			});
		}
	};
}]).
directive('countryPhoneCodeWidget',['countriesOptions',function(countriesOptions){
    return{
        restrict:'AEC',
        require:'?ngModel',
        replace:true,
        template:'<select id="country_phone_code" ng-model="customer.country_phone_code" ng-options="country.name_country_phone_code for country in countries"></select>',
        link:function(scope, element, attrs, ctrl){
            scope.countries = countriesOptions.en;
            scope.customer.country_phone_code = scope.countries[137];
        }
        
    }

}]).
directive('countryWidget',['countriesOptions',function(countriesOptions){
    return{
        restrict:'AEC',
        require:'?ngModel',
        replace:true,
        template:'<select id="country" ng-model="customer.country" ng-options="country.name for country in countries"></select>',
        link:function(scope, element, attrs, ctrl){
            scope.countries = countriesOptions.en;
            scope.customer.country = scope.countries[137];
        }
        
    }

}]).
directive('autoCompleteInput', [function () {
	return {
		restrict: 'AEC',
		require:'ngModel',
		link: function (scope, element, iAttrs, ctrl) {
			$.widget("custom.catcomplete", $.ui.autocomplete, {
                _create:function(){
                    this._super();
                    this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
                },
				_renderMenu:function(ul,items){
					var that = this;
					var currentCategory="";
					$.each(items, function(index, item){
						if(item.category != currentCategory){
                            var li = document.createElement('li');
                            li.innerHTML = item.category;
							ul.append(li);
                            li.className ="ui-autocomplete-category";
							currentCategory = item.category;
						}
						that._renderItemData(ul, item);
					});
				}
			});
            $.ui.autocomplete.filter = function (array, term) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
                return $.grep(array, function (value) {
                    return matcher.test(value.label || value.value || value);
                });
            };
            
            scope.makeCustomItem = function(item){
                if(item !== null && typeof item !== 'undefined'){
                    return{
                        'label': item.label + ", " + item.lugar,
                        'category': item.category,
                        'code': item.code,
                        'name': item.label,
                        'url': item.url,
                        'iata': item.iata,
                        'refpoint': item.refpoint,
                        'idciudad': item.idciudad
                    };
                }	
            }
			$.fn.sort = function(){
				return this.pushStack([].sort.apply(this, arguments), []);
			}
            
			scope.sortLastCategory = function(currentElement, nextElement){
				if(currentElement.category == nextElement.category) return 0;
				return currentElement.category > nextElement.category ? 1 : -1;
			}
            
            //renderizado del componente
			element.catcomplete({
				delay:0,
				minLength:3,
				source:function(request, response){
					$.ajax({
						url:'http://greenjay.univisit.com/DesktopModules/UVRates/JSON.ashx',
						dataType:'jsonp',
                        data:{'oper':'GetCities', 'name_startsWith':request.term, 'type':'Hotel'},
						success:function(data){
                            if(data.results !== null && typeof data.results !== 'undefined' && data.results.length > 0){
                                var sorted = $(data.results).sort(scope.sortLastCategory);
                                var places = $.map(sorted,  scope.makeCustomItem);
                                response($.ui.autocomplete.filter(places,request.term));
                            }else{
                                response( [{'label':'0 resultados.', 'category':'Sin resultados.'}]);
                            }
                            
                        },
						error:function(error){
                            console.log(error);
                        }
					});
				},
				select:function(event, ui){
                    if(ui.item){
                        ctrl.$setViewValue({
                            'Iata':ui.item.iata, 
                            'RefPoint':ui.item.refpoint, 
                            'Redirect':ui.item.url, 
                            'Category':ui.item.category, 
                            'Code':ui.item.code, 
                            'idciudad':ui.item.idciudad
                        });
                    } 
                },
                focus:function(event, ui){
                    if(ui.item !== null && typeof ui.item !== 'undefined'){
                        element.val(ui.item.label);
                        return false;
                    }
                }
			});    
		}
	};
}])
.directive('starsFilterWidget',[function(){
    return {
        restrict:'AEC',
        templateUrl:'../templates/stars-filter.html',
        controller:['$scope', '$element', function($scope, $element){
            
            $scope.starsSelectedCollection = [];
            
            $scope.onFilterStarSelected = function(star){
                var index = _.indexOf($scope.starsSelectedCollection, star);
                if(index > -1){
                    $scope.starsSelectedCollection.splice(index, 1);
                }else{
                    $scope.starsSelectedCollection.push(star);
                }
                $scope.filteringHotels();
            };
            
            $scope.filterHotelByStars = function(Hotel){
                if($scope.starsSelectedCollection.length > 0){
                    return _.some($scope.starsSelectedCollection, function(starSelected){
                        return Hotel.Category === starSelected;
                    });     
                }
                return true;
            };
            
            $scope.hasStar = function(star){
               return (_.indexOf($scope.starsSelectedCollection, star) > -1);
            };
            
            $scope.$watch('hotelsCollection', function(collection){
                if(collection !== null && typeof collection !== 'undefined' && collection.length > 0){
                     $scope.starsCategoryCollection = _.pluck(_.uniq(collection, 'Category'),'Category');
                }
            })
            
        }],
        link:function(scope, element, attrs){
           
        }
        
    }
    
}])
.directive('marketsFilterWidget', [function(){

}])
.directive('nameFilterWidget',[function(){
    return{
        restrict:'AEC',
        controller:['$scope','$element', function($scope, $element){
            $scope.filterHotelByName = function(Hotel){
                if($scope.filterName !== null && typeof $scope.filterName !== 'undefined'){
                    return _.contains(Hotel.Name.toLocaleLowerCase(), $scope.filterName.toLowerCase());
                }
                return true;
            };
        }],
        link:function(scope, element, attrs){
            element.bind('keyup', function(e){
                e.preventDefault();
                scope.$apply(function(){
                    scope.filteringHotels();
                })
            })
        }
    }
}])
.directive('priceRangeFilterWidget',[function(){
    return{
        restrict:'AEC',
        controller:['$scope','$element', function($scope, $element){
            $scope.$watch('hotelsCollection', function(collection){
               if(collection !== null && typeof collection !== 'undefined' && collection.length > 0){
                    $scope.min = _.min(collection,'AltLowRate').AltLowRate
                    $scope.max = _.max(collection,'AltLowRate').AltLowRate;
                    $scope.minPriceRange = $scope.min;
                    $scope.maxPriceRange = $scope.max;
               }
            });
            
            $scope.filterHotelByPriceRange = function(Hotel){
                if($scope.minPriceRange !== $scope.min || $scope.maxPriceRange !== $scope.max){
                    return (Hotel.AltLowRate >= $scope.minPriceRange && Hotel.AltLowRate <= $scope.maxPriceRange);
                }
                return true;
            }
        }],
        link:function(scope, element, attrs){
            scope.$watch('min + max', function(value){
                element.slider({
                  range: true,
                  min: scope.min,
                  max: scope.max,
                  values: [ scope.min, scope.max],
                  slide: function( event, ui ) {
                        scope.$apply(function(){
                            scope.minPriceRange = ui.values[ 0 ];
                        scope.maxPriceRange = ui.values[ 1 ];
                        scope.filteringHotels();
                        })
                        
                  }
                });
            });
             
        }
    }
}])
.directive('paginationWidget', ['paginationConfig','$parse','$interpolate',function(config, $parse, $interpolate){
    return{
        restrict:'AE',
        replace:true,
        templateUrl:'../templates/paginator.html',
        link:function(scope, element, attrs, paginationCtrl){
              // Setup configuration parameters
            var maxSize,
            boundaryLinks = paginationCtrl.getAttributeValue(attrs.boundaryLinks, config.boundaryLinks),
            directionLinks = paginationCtrl.getAttributeValue(attrs.directionLinks, config.directionLinks),
            firstText = paginationCtrl.getAttributeValue(attrs.firstText, config.firstText, true),
            previousText = paginationCtrl.getAttributeValue(attrs.previousText, config.previousText, true),
            nextText = paginationCtrl.getAttributeValue(attrs.nextText, config.nextText, true),
            lastText = paginationCtrl.getAttributeValue(attrs.lastText, config.lastText, true),
            rotate = paginationCtrl.getAttributeValue(attrs.rotate, config.rotate);

            paginationCtrl.init(config.itemsPerPage);

            if (attrs.maxSize) {
                scope.$parent.$watch($parse(attrs.maxSize), function (value) {
                    maxSize = parseInt(value, 10);
                    paginationCtrl.render();
                });
            }

            // Create page object used in template
            function makePage(number, text, isActive, isDisabled) {
                return {
                    number: number,
                    text: text,
                    active: isActive,
                    disabled: isDisabled
                };
            }
            
             paginationCtrl.getPages = function (currentPage, totalPages) {
                var pages = [];

                // Default page limits
                var startPage = 1, endPage = totalPages;
                var isMaxSized = (angular.isDefined(maxSize) && maxSize < totalPages);

                // recompute if maxSize
                if (isMaxSized) {
                    if (rotate) {
                        // Current page is displayed in the middle of the visible ones
                        startPage = Math.max(currentPage - Math.floor(maxSize / 2), 1);
                        endPage = startPage + maxSize - 1;

                        // Adjust if limit is exceeded
                        if (endPage > totalPages) {
                            endPage = totalPages;
                            startPage = endPage - maxSize + 1;
                        }
                    } else {
                        // Visible pages are paginated with maxSize
                        startPage = ((Math.ceil(currentPage / maxSize) - 1) * maxSize) + 1;

                        // Adjust last page if limit is exceeded
                        endPage = Math.min(startPage + maxSize - 1, totalPages);
                    }
                }

                // Add page number links
                for (var number = startPage; number <= endPage; number++) {
                    var page = makePage(number, number, paginationCtrl.isActive(number), false);
                    pages.push(page);
                }

                // Add links to move between page sets
                if (isMaxSized && !rotate) {
                    if (startPage > 1) {
                        var previousPageSet = makePage(startPage - 1, '...', false, false);
                        pages.unshift(previousPageSet);
                    }

                    if (endPage < totalPages) {
                        var nextPageSet = makePage(endPage + 1, '...', false, false);
                        pages.push(nextPageSet);
                    }
                }

                // Add previous & next links
                if (directionLinks) {
                    var previousPage = makePage(currentPage - 1, previousText, false, paginationCtrl.noPrevious());
                    pages.unshift(previousPage);

                    var nextPage = makePage(currentPage + 1, nextText, false, paginationCtrl.noNext());
                    pages.push(nextPage);
                }

                // Add first & last links
                if (boundaryLinks) {
                    var firstPage = makePage(1, firstText, false, paginationCtrl.noPrevious());
                    pages.unshift(firstPage);

                    var lastPage = makePage(totalPages, lastText, false, paginationCtrl.noNext());
                    pages.push(lastPage);
                }

                return pages;
            };
            
            
        },
        controller:['$scope','$element','$attrs', function($scope, $element, $attrs){
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 20;
            $scope.page = $scope.currentPage;
            
            $scope.$watch('hotelsCollection', function(collection){
               if(collection !== null && angular.isDefined(collection) && collection.length > 0){
                    $scope.totalItems= collection.length;
                    $scope.paginate();
               }        
            });
            
            var self = this,setNumPages = $attrs.numPages ? $parse($attrs.numPages).assign : angular.noop;

            this.init = function (defaultItemsPerPage) {
                if ($attrs.itemsPerPage) {
                    $scope.$parent.$watch($parse($attrs.itemsPerPage), function (value) {
                        self.itemsPerPage = parseInt(value, 10);
                        $scope.totalPages = self.calculateTotalPages();
                    });
                } else {
                    this.itemsPerPage = defaultItemsPerPage;
                }
            };

            this.noPrevious = function () {
                return this.page === 1;
            };
            this.noNext = function () {
                return this.page === $scope.totalPages;
            };

            this.isActive = function (page) {
                return this.page === page;
            };

            this.calculateTotalPages = function () {
                var totalPages = this.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / this.itemsPerPage);
                return Math.max(totalPages || 0, 1);
            };

            this.getAttributeValue = function (attribute, defaultValue, interpolate) {
                return angular.isDefined(attribute) ? (interpolate ? $interpolate(attribute)($scope.$parent) : $scope.$parent.$eval(attribute)) : defaultValue;
            };

            this.render = function () {
                this.page = parseInt($scope.page, 10) || 1;
                if (this.page > 0 && this.page <= $scope.totalPages) {
                    $scope.pages = this.getPages(this.page, $scope.totalPages);
                }
            };

            $scope.selectPage = function (page) {
                if (!self.isActive(page) && page > 0 && page <= $scope.totalPages) {
                    $scope.page = page;
                    $scope.onSelectPage({ page: page });
                }
            };
            
            $scope.onSelectPage = function(page){
                $scope.currentPage = page.page;
            }
            $scope.$watch('currentPage', function(){
                $scope.paginate();
            })

            $scope.$watch('page', function () {
                self.render();
            });

            $scope.$watch('totalItems', function () {
                $scope.totalPages = self.calculateTotalPages();
            });

            $scope.$watch('totalPages', function (value) {
                setNumPages($scope.$parent, value); // Readonly variable
                if (self.page > value) {
                    $scope.selectPage(value);
                } else {
                    self.render();
                }
            });
            
            $scope.paginate = function (){
                var begin = (($scope.currentPage - 1) * $scope.itemsPerPage), end = begin + $scope.itemsPerPage;
                $scope.hotelsCollectionShowed = ($scope.isFiltered)?$scope.filteredCollection.slice(begin,end):$scope.hotelsCollection.slice(begin, end);
                $scope.totalPages = self.calculateTotalPages();
            }
            
        }]
    }
}])
.directive('hotelItem',[function(){
    return{
        restrict:'AEC',
        templateUrl:'../templates/hotel-item.html',
        link:function(scope, element, iAttrs){
            
        },
        controller:['$scope', '$element', function($scope, $element){
        
        }]
    }
}])
.directive('bookingBoxWidget', ['Utils','BookingBoxConfig','BookingBox', function (Utils, BookingBoxConfig, BookingBox) {
	return {
		restrict: 'AEC',
		templateUrl:'../templates/bookingbox.html',
		link: function (scope, element, iAttrs) {
			scope.generateRoomsOptions();
			scope.generateAdultsOptions();
			scope.generateChildrenOptions();
			scope.generateAgesOptions();
			scope.roomOptionChange(Utils.getParameterURL('Rooms') || BookingBoxConfig.MIN_ROOMS);
            scope.bindBookingBox();
		},
		controller:['$scope', '$element', function($scope, $element){
			
			
            $scope.generateRoomsOptions = function(){
                $scope.roomsOptions = [];
				for (var i = BookingBoxConfig.MIN_ROOMS; i <= BookingBoxConfig.MAX_ROOMS; i++) {
					$scope.roomsOptions.push(i);
				};
			};
	
			$scope.generateAdultsOptions = function(){
				$scope.adultsOptions = [];
				for (var i = BookingBoxConfig.MIN_ADULTS; i <= BookingBoxConfig.MAX_ADULTS; i++) {
					$scope.adultsOptions.push(i);
				};
			};
			
			$scope.generateChildrenOptions = function(){
				$scope.childrenOptions = [];
				for (var i = BookingBoxConfig.MIN_CHILDREN; i <= BookingBoxConfig.MAX_CHILDREN; i++) {
					$scope.childrenOptions.push(i);
				};

			};

			$scope.generateAgesOptions = function(){
				$scope.agesOptions = [];
				for (var i = BookingBoxConfig.MIN_CHILDREN_AGES; i <= BookingBoxConfig.MAX_CHILDREN_AGES; i++) {
					$scope.agesOptions.push(i);
				};
			};

			$scope.roomOptionChange = function(roomOption){
				$scope.bookingBoxDataStructure = [];
				for (var i = BookingBoxConfig.MIN_ROOMS; i <= roomOption; i++) {
					$scope.bookingBoxDataStructure.push({
						'label':('Habitación '+i+':'),
						'adultsSelected':$scope.adultsOptions[0],
						'childrenSelected':$scope.childrenOptions[0],
						'childrenAgesStructure':[]
					});
				};
                $scope.roomOptionSelected = roomOption;
			};

			$scope.childrenOptionChange = function(roomNumber, childrenOption){
				$scope.bookingBoxDataStructure[roomNumber].childrenAgesStructure = [];
				for (var i = BookingBoxConfig.MIN_CHILDREN_AGES; i < childrenOption; i++) {
					$scope.bookingBoxDataStructure[roomNumber].childrenAgesStructure.push({
						'ageSelected':0,
						'label':'Niño' + (i+1)
					});
				};
			};

			$scope.send = function(){
				$scope.Rooms = $scope.bookingBoxDataStructure.length;
				var adults = [];
				var children = [];
				var ages = [];
				for (var i = 0; i < $scope.bookingBoxDataStructure.length; i++) {
					adults.push($scope.bookingBoxDataStructure[i].adultsSelected);
					children.push($scope.bookingBoxDataStructure[i].childrenSelected);
					for (var j = 0; j < $scope.bookingBoxDataStructure[i].childrenAgesStructure.length; j++) {
						ages.push($scope.bookingBoxDataStructure[i].childrenAgesStructure[j].ageSelected);
					};
				};
				$scope.Adults = adults.join(',');
				$scope.Children = children.join(',');
				$scope.Ages = ages.join(',');
				$scope.Iata = $scope.Destination.Iata || '';
				$scope.RefPoint = $scope.Destination.RefPoint || '';
			}
            
            $scope.bindBookingBox = function(){
                var agesIndex = 0;
                for(var i = 0; i< $scope.bookingBoxDataStructure.length; i++){
                    $scope.bookingBoxDataStructure[i].adultsSelected = $scope.getParameterByIndex(i, 'Adults', 'adultsOptions');   
                    $scope.bookingBoxDataStructure[i].childrenSelected = $scope.getParameterByIndex(i, 'Children', 'childrenOptions');
                    $scope.childrenOptionChange(i, $scope.bookingBoxDataStructure[i].childrenSelected);
                    if(Utils.getParameterURL('Ages') !== null && angular.isDefined(Utils.getParameterURL('Ages'))){
                        for(var j = 0; j < $scope.bookingBoxDataStructure[i].childrenSelected; j++){
                            $scope.bookingBoxDataStructure[i].childrenAgesStructure[j].ageSelected = parseInt(Utils.getParameterURL('Ages').split(',')[agesIndex]);
                            agesIndex++;
                        }
                    }
                    
                }
            }
            
            $scope.getParameterByIndex = function(index, parameter, arrayOption){
                var parameterData = Utils.getParameterURL(parameter);
                if(parameterData)
                    return parseInt(parameterData.split(',')[index]);
                else
                    return $scope[arrayOption][0];
            };
            
            $scope.areParametersWell = function(){
                var result = (Utils.getParameterURL('Rooms') !== null && (Utils.getParameterURL('Rooms').split(','.lengh === 1)))
                return (Utils.getParameterURL('Adults' || 'adults').split(',').length === Utils.getParameterURL('Children' || 'children').split(',').length)
            }
			
		}]
	};
}])