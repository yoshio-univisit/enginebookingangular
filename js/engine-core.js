//Modulo Engine Core 
/*Este modulo contiene servicios para obtener información del BookingBox, almacenar la informacion del contacto de reservacion, los datos del pago de reservacion, obtener los hoteles con sus tarifas ya procesadas basados en el factory HotelModel e implementa utilidades para tareas como obtener los valores de parametros, estandarizar las urls de las estrellas
*/
"use strict";
angular.module('EngineCore', [])
.config(['$provide','$httpProvider', function ($provide, $httpProvider) {
    //$httpProvider.interceptors.push('requestRejector');
    //$httpProvider.interceptors.push('requestRecoverer');
	$httpProvider.interceptors.push('loaderInterceptor');
    //provider para transformar XML a JSON
    $provide.factory('XMLtoJSON', [function () {
		return new X2JS();
	}]);
    
    //provider para conocer los eventos de autenticación
    $provide.constant('AUTH_EVENTS',{
        loginSuccess:'auth-login-success',
        loginFailed:'auth-login-failed',
        logoutSuccess:'auth-logout-success',
        sessionTimeout:'auth-session-timeout',
        notAuthenticated:'auth-not-authenticated',
        notAuthorized:'auth-not-authorized'
    });
    
    //provider para conocer los roles de los usuario
    $provide.constant('USER_ROLES',{
        all:'*',
        client:'client',
        agency:'agency'
    });
    
    //provider status de las peticiones, manejo de errores
    $provide.constant('REQUEST_STATUS',{
        notFound:404,
        success:200
    });
    
    //provider cantidad de estrellas
    $provide.constant('POSIBLE_STARS_RESOURCE_NUMBER',[0,1,2,3,4,5]);
    
    //provider cantidad de estrellas con letras
    $provide.constant('POSIBLE_STARS_RESOURCE_LETTER',{
        'es':['cero','uno','dos','tres','cuatro','cinco'],
        'en':['zero','one','two','three','four','five']
    });
    
    //imagen por default si el hotel no tiene imagen
    $provide.constant('NO_HOTEL_IMG','img/hotel_not_yet.png');
    
    
}])
.factory('requestRejector',['$q',function($q){
    var requestRejector = {
        request:function(config){
            return $q.reject('requestRejector');
        }
    }
    return requestRejector;
}])
.factory('requestRecoverer',['$q', function($q){
    var requestRecover = {
        requestError:function(rejectReason){
            if(rejectReason === 'requestRejector'){
                return {
                    transformRequest: [],
                    transformResponse: [],
                    method: 'GET',
                    url: 'https://api.github.com/users/naorye/repos',
                    headers: {
                        Accept: 'application/json, text/plain, */*'
                    }
                }
            }else{
                return $q.reject(rejectReason);
            }
        }
    }
    return requestRecover;
}])
.factory('loaderInterceptor',['$q',function($q){
    var loaderInterceptor = {
        request:function(config){
             console.log(config);
             return config || $q.when(config);
        },
        response:function(response){
            //console.log(response);
            return response || $q.when(response);
        },
        requestError:function(rejectReason){
            return $q.reject(rejectReason);
        },
        responseError :function(rejectReason){
            return $q.reject(rejectReason);
        }
    };
    return loaderInterceptor;
}])
.service('BookingBox', ['Utils', function(Utils){
	//Datos basicos de busqueda
    this.destination = Utils.getParameterURL('Destination');
	this.checkIn = Utils.getParameterURL('CheckIn');
	this.checkOut = Utils.getParameterURL('CheckOut');
	this.roomsQuantity = Utils.getParameterURL('Rooms');
	this.adultsQuantity = Utils.getParameterURL('Adults');
	this.childrenQuantity = Utils.getParameterURL('Children');
	this.childrenAges = Utils.getParameterURL('Ages');
	this.iata = Utils.getParameterURL('Iata');
	this.refPoint = Utils.getParameterURL('RefPoint');
	this.currency = Utils.getParameterURL('Currency');
	this.propertyNumber = Utils.getParameterURL('PropertyNumber');
}]).service('ContactReservation',[function(){
    //datos basicos para el contacto en la reservacion
	this.fullName;
	this.email;
	this.phone;
	this.address;
}]).service('PaymentReservation',[function(){
	this.paymentType;
	this.cardType;
	this.cardNumber;
	this.securityCode;
	this.cardOwner;
	this.expirationDate;
}]).service('HotelsService',['BookingBox','HotelModel','$http','XMLtoJSON','REQUEST_STATUS', function(BookingBox, HotelModel, $http, XMLtoJSON, REQUEST_STATUS){
	
    //coleccion de hoteles
    var hotelCollection = [];

    //promesea para obtener hoteles
	this.getHotels = function(){
		return univisitHotels()
		.then(hotelBedsHotels)
		.then(function(){
			return hotelCollection;
		});
	};
    
    //promesa para obtener la disponibilidad de hoteles
    this.getHotelsAvailability = function(){
        return univisitAvailability()
        .then(hotelBedsAvailability)
        .then(function(){
            return hotelCollection;
        });
    }
    
    //promesa para la peticion de hoteles propios univisit
	var univisitHotels = function(){
		return $http({
			'url':'../data/univisit-hotels-response.json',
			'method':'GET'
		}).then(hotelResponse, errorResquestHandling);
	}
    //promesa para la peticion de hoteles del proveedor HotelBeds
	var hotelBedsHotels = function(){
		return $http({
			'url':'../data/Hotels.xml',
			'method':'GET'
		}).then(hotelResponse, errorResquestHandling);
	};
    
    //promesa para obtener la disponibilidad de hoteles de univisit
	var univisitAvailability = function(){
		return $http({
			'url':'../data/hotelsbeds-availability-response.json',
			'method':'GET'
		}).then(hotelResponse, errorResquestHandling);	
	};
    
    //promesa para obtener la disponibilidad de hoteles del proveedor hotelbeds
	var hotelBedsAvailability = function(){
		return $http({
			'url':'../data/Rates.xml',
			'method':'GET'
		}).then(availabilityResponse, errorResquestHandling);	
	};

    //tratamiento de datos de la respuesta a la petición de hoteles
	var hotelResponse = function(response){
		if(response.data !== null && angular.isDefined(response.data)){
            var data = (angular.isString(response.data))? XMLtoJSON.xml_str2json(response.data):response.data;
            if(data.Hotel_HOI_RS !== null && angular.isDefined(data.Hotel_HOI_RS)){
				var hotelHoiRs = data.Hotel_HOI_RS;
				if(hotelHoiRs.Properties !== null 
					&& angular.isDefined(hotelHoiRs.Properties)){
					var Properties = hotelHoiRs.Properties;
					if(Properties.Property !== null && angular.isDefined(Properties.Property)
						&& Properties.Property.length > 0){
						angular.forEach(Properties.Property, function(item, index){
							hotelCollection.push(new HotelModel({
                                'PropertyNumber':item.PropertyNumber,
								'Name':item.PropertyName,
								'Category':item.Category,
								'Description':item.ShortDescription,
								'Address':item.Address,
								'CityName':item.CityName,
								'Image':item.ImageURL || item.LogoURL || ""
							}))
						})
					}
				}
			}
		}
	};

    //tratamiento de datos de la respuesta a la peticion de diponibilidad de hoteles
	var availabilityResponse = function(response){
        if(response.data !== null && angular.isDefined(response.data)){
            var data = (angular.isString(response.data))? XMLtoJSON.xml_str2json(response.data):response.data;
            if(data.Hotel_HOI_RS !== null && angular.isDefined(data.Hotel_HOI_RS)){
                var hotelHoiRs = data.Hotel_HOI_RS;
				if(hotelHoiRs.Properties !== null 
					&& angular.isDefined(hotelHoiRs.Properties)){
					var Properties = hotelHoiRs.Properties;
					if(Properties.Property !== null && angular.isDefined(Properties.Property)
						&& Properties.Property.length > 0){
                        angular.forEach(hotelCollection, function(Hotel, index){
							var temporaryHotel = _.where(Properties.Property, {'PropertyNumber':Hotel.PropertyNumber})[0];
                            if(temporaryHotel){
                                Hotel.LowRate = parseFloat(temporaryHotel.LowRate || null);
                                Hotel.AltLowRate = parseFloat(temporaryHotel.AltLowRate || null);
                                Hotel.Currency = temporaryHotel.Currency || '';
                                Hotel.AltCurrency = temporaryHotel.AltCurrency || '';
                            }
						});
					}
				}
            }
        }
        
        hotelCollection = _.filter(hotelCollection,function(item){
            return item.AltLowRate !== null && angular.isDefined(item.AltLowRate) && item.AltLowRate > 0;
        })
	};
    
    //funcion para el manejo de errores en las peticiones
    var errorResquestHandling = function(response){
        switch(response.status){
            case REQUEST_STATUS.notFound:
                console.log(response.statusText);
                break;
        };
    };

}]).factory('HotelModel', ['Utils', function (Utils) {
	return function(parameters){
        this.PropertyNumber = parameters.PropertyNumber || null;
		this.Name = parameters.Name || '';
		this.Category = Utils.parseCategory(parameters.Category || 'none') ;
		this.Description = parameters.Description || '';
		this.Address = parameters.Address || '';
		this.CityName = parameters.CityName || '';
		this.Image = Utils.parseImage(parameters.Image);
        this.LowRate = parameters.LowRate || null;
        this.AltLowRate = parameters.AltLowRate || null;
        this.Currency =  parameters.Currency || null;
        this.AltCurrency = parameters.AltCurrency || null;
	};
}]).service('Utils', ['POSIBLE_STARS_RESOURCE_NUMBER','POSIBLE_STARS_RESOURCE_LETTER','NO_HOTEL_IMG',
    function (POSIBLE_STARS_RESOURCE_NUMBER, POSIBLE_STARS_RESOURCE_LETTER, NO_HOTEL_IMG){
	
    this.getParameterURL = function(parameterName){
		return decodeURIComponent((new RegExp('[?|&]' + parameterName + '=' 
		+ '([^&;]+?)(&|#|;|$)').exec(location.search) 
		|| [, ""])[1].replace(/\+/g, '%20')) || null;
	};
    
    this.parseCategory = function(resource){
        if(resource !== null && typeof resource !== 'undefined'){
            resource = resource.toLowerCase();
            return (_.contains(resource,'http'))? parseStar(_.last(resource.split('/'))): parseStar(resource);
        }
        return "star-none"
    };
    
    var parseStar = function(resource){
        if(_.contains(resource,'star') || _.contains(resource,'estrella')){
            var temporalStar = _.filter(POSIBLE_STARS_RESOURCE_NUMBER, function(star, index){
                return _.contains(resource,star) 
                || _.contains(resource,POSIBLE_STARS_RESOURCE_LETTER.es[index]) 
                || _.contains(resource,POSIBLE_STARS_RESOURCE_LETTER.en[index]);
            });
            return "star-".concat(temporalStar[0]);
        }else if(_.contains(resource,'empty')){
            return "star-none"
        }   
        return resource;
    };
    
    this.parseImage = function(resource){
        if(resource !== null && typeof resource !== 'undefined'){
            resource = resource.toLowerCase();
            return (_.contains(resource,'empty'))? NO_HOTEL_IMG : resource;
        }
        return NO_HOTEL_IMG;
    };
    
}]);









