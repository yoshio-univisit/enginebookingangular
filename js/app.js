"use strict";
angular.module('EngineUnivisit', ['EngineCore','EngineWidgets', 'ngAnimate', 'ui.router'])
.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){
    $stateProvider
		.state('form', {
			url: '/reservation',
			templateUrl: '../templates/reservation/reservation.html',
			controller: 'PaymentProcessCtrl'
		})
        .state('form.reservationdetail', {
			url: '/reservationdetail',
			templateUrl: '../templates/reservation/reservation-details.html',
		})
		.state('form.customerprofile', {
			url: '/customerprofile',
			templateUrl: '../templates/reservation/reservation-customer-information.html',
		})
		.state('form.payment', {
			url: '/payment',
			templateUrl: '../templates/reservation/reservation-payment-information.html',
            resolve:{
                customerValidated:function(){
                    return false;
                }
            }
		})
		.state('form.paymentsuccess', {
			url: '/paymentsuccess',
			templateUrl: '../templates/reservation/reservation-payment-success.html',
		})
        .state('form.error', {
                url: '/paymenterror',
                templateUrl: '../templates/reservation/reservation-payment-error.html',
            });
	$urlRouterProvider.otherwise('/reservation/reservationdetail');
}])
.controller('HotelListCtrl', ['$scope','$filter','BookingBox','HotelsService', function ($scope, $filter, BookingBox, HotelsService) {
	
    //cache de colección de hoteles
    $scope.hotelsCollection = [];
    //coleccion de hoteles a mostrar
    $scope.hotelsCollectionShowed = [];
    //coleccion temporal de hoteles filtrados
    $scope.FilteredCollection = [];
	$scope.isFiltered = false;
    
    HotelsService.getHotels()
    .then(HotelsService.getHotelsAvailability)
    .then(function(response){
        $scope.hotelsCollection = response;
    });
    
    $scope.filteringHotels = function(){
       $scope.filteredCollection = _.filter($scope.hotelsCollection, function(Hotel){
            return $scope.filterHotelByName(Hotel)
                && $scope.filterHotelByStars(Hotel)
                && $scope.filterHotelByPriceRange(Hotel);
       });
        $scope.totalItems = $scope.filteredCollection.length;
        $scope.isFiltered = ($scope.hotelsCollection.length !== $scope.filteredCollection.length);
        $scope.currentPage = 1;
        $scope.paginate();
    };
    
    
    
    
}])
.controller('HotelDetailCtrl', ['$scope', function ($scope) {
	
}])
.controller('PaymentProcessCtrl', ['$scope', function ($scope) {
	$scope.customer = {
        first_name:'',
        last_name:'',
        email:'',
        country_phone_code:null,
        phone:null
    };
}]);